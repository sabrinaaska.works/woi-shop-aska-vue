import Vuex from "vuex";

import state from "./state";
import * as getters from "./getters.js";
import * as mutations from "./mutations.js";
import * as actions from "./actions.js";

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
});
