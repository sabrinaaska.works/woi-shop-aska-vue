import "./interceptors/axios";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import store from "./store";
import Vuex from "vuex";
import "./style.css";
import "./index.css";
import "flowbite";

createApp(App).use(router).use(store).use(Vuex).mount("#app");
