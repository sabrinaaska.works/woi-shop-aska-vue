import { createWebHistory, createRouter } from "vue-router";

import ComponentLogin from "../components/login/Component.vue";
import ComponentSignup from "../components/signup/Component.vue";
import ComponentVerification from "../components/verification/Component.vue";
import ComponentCategory from "../components/category/Component.vue";
import ComponentProductList from "../components/product-list/Component.vue";
import ComponentProductDetail from "../components/product-detail/Component.vue";
import ComponentCart from "../components/cart/Component.vue";

import ContentLoggedin from "../components/loggedin/Component.vue";

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    component: ComponentLogin,
  },
  {
    path: "/register",
    component: ComponentSignup,
  },
  {
    path: "/verification",
    component: ComponentVerification,
  },
  {
    path: "/category",
    component: ComponentCategory,
  },
  {
    path: "/category/:category",
    component: ComponentProductList,
  },
  {
    path: "/category/:category/:id",
    component: ComponentProductDetail,
  },
  {
    path: "/cart",
    component: ComponentCart,
  },
  {
    path: "/loggedin",
    component: ContentLoggedin,
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
