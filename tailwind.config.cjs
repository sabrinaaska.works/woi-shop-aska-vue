module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        100: "#ffffff",
        200: "#292929",
        300: "#7a7a7a",
        400: "#03ac0e",
        500: "#049c0e",
        600: "#F5F5F5",
        700: "#333333",
        800: "#ED0A2A",
        900: "#EBEBEB",
        1000: "rgba(0, 0, 0, 0.04)",
        1100: "#FAFAFA",
        1200: "rgba(3, 172, 14, 0.22)",
        1300: "rgb(3 172 14 / 24%)",
        1400: "rgba(0, 0, 0, 0.23)",
        1500: "#CCCCCC",
      },
      gridTemplateColumns: {
        category: "repeat(auto-fill, minmax(90px, 1fr))",
        basecategory: "repeat(auto-fill, minmax(85px, 1fr))",
        smallcategory: "repeat(auto-fill, minmax(70px, 1fr))",
        products: "repeat(auto-fill, minmax(125px, 1fr))",
      },
    },
    maxWidth: {
      90: "90px",
      500: "500px",
      "3/4": "75%",
      "3/12": "25%",
    },
    minHeight: {
      120: "120vh",
    },
    boxShadow: {
      1000: "0px 2px 8px",
      1300: "0px 8px 16px",
    },
    screens: {
      base: { max: "500px" },
      small: { max: "380px" },
      smallest: { max: "285px" },
    },
  },
  plugins: [],
};
